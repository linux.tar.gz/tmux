#!/bin/bash

for pane in $(tmux list-panes -F '#P')
do
	tmux send-keys -t "$pane" "reset && . ~/.bashrc" Enter
done
